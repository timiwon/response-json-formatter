# Resopnse json formatter

API response json format

## Usage

### init formatter

formatter/index.js
```
const formatter = require('response-json-formatter');
module.exports = formatter;
```

### errorHandler middleware

index.js
```
const {
    errorHandler,
} = require('../formatter');
const express = require('express');

const app = express()
...
app.use(errorHandler(logOptions));
```

### validate middleware
route.js
```
const { Router } = require('express');
const router = new Router();
const Joi = require('joi');
const {
    validate
} = require('../formatter');

const pathSchema = Joi.object().keys({
    locationIdClient: Joi.string().required(),
});

router.post('/',
    validate(createSchema, 'body'),
    createController)
``` 

### response format modules
controller.js
```
const {
    okData,
    okDataList,
    okCreated,
} = require('../formatter');

const getItem = async (req, res, next) => {
    ...
    okData(res, item);
};

const getList = async (req, res, next) => {
    ...
    okDataList(res, list, { total: list.length });
};

const create = async (req, res, next) => {
    ...
    okCreated(res, data);
};
```
### error instances
```
const {
    ApiError,
    ApiValidateError,
    ConflictedModificationError,
    ForbiddenAccessError,
    InternalTechnicalError,
    InvalidAuthenticationError,
    NotFoundError
} = require('../formatter');
```
