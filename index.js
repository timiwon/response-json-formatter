const validate = require('./lib/middleware/validate');

const Errors = require('./lib/error');
const formatters = require('./lib/formatter');

let errorHandlerMiddleware = require('./lib/middleware/errorHandler');
let log = require('./lib/logger');

const errorHandler = logOptions => {
    log = log(logOptions);
    return errorHandlerMiddleware(log);
}

module.exports = {
    log,
    validate,
    errorHandler,
    ...formatters,
    ...Errors,
};
